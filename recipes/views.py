from django.shortcuts import get_list_or_404, get_object_or_404, redirect, render
from recipes.models import Recipe
from .forms import RecipeForm

# Create your views here.

def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": a_really_cool_recipe
    }
    # print(context)
    return render(request, 'recipes/detail.html', context)

def list_recipes(request):
    really_cool_recipes = Recipe.objects.all()
    context = {
        "recipes": really_cool_recipes
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("recipes_list")
    else:
        form = RecipeForm()
    context = {"form": form}
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    # post = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("recipes_list")
    else:
        form = RecipeForm(instance=post)
    context = {"form": form, "recipe": post}
    return render(request, "recipes/edit.html", context)
