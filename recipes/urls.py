from django.urls import path
from recipes.views import show_recipe, list_recipes, create_recipe, edit_recipe


urlpatterns = [
    path("", list_recipes, name="recipes_list"),
    path("create/", create_recipe, name = "create_a_really_delicious_recipe"),
    path("<int:id>/", show_recipe, name = "show_recipe"),
    path("<int:id>/edit/", edit_recipe, name = "edit_recipe"),
]
